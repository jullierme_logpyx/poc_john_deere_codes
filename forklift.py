from Mqtt_Interface import Mqtt_Interface
from process_data import TAGS_DATA,security_distance, process_data
from Rasp_GPIO import Rasp_GPIO
from forklift_position import forklift_position
from send_to_cloud import Send_to_cloud_Mqtt
import time
import serial
import json

from binascii import unhexlify

alarm_timeout = 3 #seconds

send_to_cloud_period = 1 #seconds

if __name__ == "__main__":

    try:
        forklift_position_data = forklift_position()
        communication_MQTT_forklift = Mqtt_Interface()
        time.sleep(0.100)
        AlarmOff = Rasp_GPIO()
        send_data_to_cloud = Send_to_cloud_Mqtt()

        time_send_to_cloud=time.time()
        while True:
            if( time.time() - communication_MQTT_forklift.time_reference_variable > alarm_timeout):
                #print("alarmOFF")
                AlarmOff.alarm_Off()
                #time.sleep(1)
            forklift_position_data.get_forklift_position()
            
            if( time.time() - time_send_to_cloud > send_to_cloud_period):
                send_data_to_cloud.send_message_to_cloud(send_data_to_cloud.client,forklift_position_data,security_distance,TAGS_DATA)
                time_send_to_cloud = time.time()

    except KeyboardInterrupt:   # Ctrl+C
        forklift_position_data.deinit_forklift_position