import RPi.GPIO as GPIO
import time

class Rasp_GPIO:

    def __init__(self):
        self.oi = ""
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(40, GPIO.OUT, initial=GPIO.LOW)
        #GPIO.setup(37, GPIO.OUT, initial=GPIO.HIGH)

    def alarm_On(self):
        GPIO.output(40, GPIO.HIGH)
        #print("alarm_on!!!")

    def alarm_Off(self):
        GPIO.output(40, GPIO.LOW)
        #print("alarm_off!!!")
