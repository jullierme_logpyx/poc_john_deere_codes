from binascii import unhexlify
import time
import math
from base64 import b64encode
from array import *

TYPE_DISTANCE=0x01
SUBTYPE_TAG_TO_ANCHOR=0x01
SUBTYPE_SECURITY_DISTANCE=0x02
SUBTYPE_TAG_TO_ANCHOR_WITH_SEC_DIST=0x03

TYPE_TAG_FIXED=0
TYPE_TAG_MOBILE=1

#MOBILE TAGS:
MOBILE_TAGS = [0x3409,0x37F5,0x3FF4,0x0CDE,0x8C8F]

#anchor
ANCHORS = [0x0437]

#global variables:
security_distance=7000

TAGS_DATA = []

class process_data:

    def data_received(self,tag_addr,payload):
        if(tag_addr in MOBILE_TAGS):
            tag_type=TYPE_TAG_MOBILE
        else:
            print("TAG NOT REGISTERED, Addr: 0x%04x" % tag_addr)
            print("")
            return {'ret_type':-1} 
        type = int.from_bytes(unhexlify(payload[0:2]), 'big')
        subtype = int.from_bytes(unhexlify(payload[2:4]), 'big')
        #print(hex(subtype))
        
        if(type==TYPE_DISTANCE):
            if(subtype==SUBTYPE_TAG_TO_ANCHOR_WITH_SEC_DIST):
                found_anchor=0
                tag_sec_distance=int.from_bytes(unhexlify(payload[4:12]), 'big')
                num_of_anchors = int.from_bytes(unhexlify(payload[12:14]), 'big')
                print("num of anchors %d" % num_of_anchors)
                for j in list(range(num_of_anchors)):
                    anchor_addr = int.from_bytes(unhexlify(payload[(j*12+14):(j*12+18)]), 'big')
                    if(anchor_addr in ANCHORS):
                        found_anchor=1
                        distance=int.from_bytes(unhexlify(payload[(j*12+18):(j*12+26)]), 'big')
                        print("ANCHOR %d %04x DISTANCE: %dmm" % (j, anchor_addr,distance))
                        break
                if (found_anchor==0): 
                    print("ANCHOR NOT FOUND")
                    return {'ret_type':-1}           
                #print(hex(anchor_addr))
                
                i=0
                tag_found=0

                for r in TAGS_DATA:
                    if(r[0]==tag_addr):
                        TAGS_DATA[i][1]=distance
                        TAGS_DATA[i][2] = round(time.time()*1000)
                        if(distance<security_distance): TAGS_DATA[i][3] = round(time.time()*1000)
                        tag_found=1
                    i=i+1
                    #print(r)
                if(tag_found==0): TAGS_DATA.insert(i, [tag_addr,distance,round(time.time()*1000),0])
                
                #Se a distância de segurança da tag é diferente da estabelecida no gateway, enviar nova mensagem com distância de segurança para a tag.
                if (security_distance != tag_sec_distance):
                    message_to_send="%02x%02x%04x%08x" % (TYPE_DISTANCE,SUBTYPE_SECURITY_DISTANCE,anchor_addr,int(security_distance))
                    message_base64 = b64encode(bytes.fromhex(message_to_send)).decode()
                    print("Sending security distance\n")
                    #return {'ret_type':1, 'all_mobile_tags':MOBILE_TAGS, 'message':str(message_base64)} #For all tags
                    return {'ret_type':1, 'mobile_tag':tag_addr, 'message':str(message_base64)} #For only one tag
                elif (distance<security_distance):
                    alarm_flag = 1
                    #print("Mobile Tag (0x%04x) Distance: %f mm (ALARM ON)" % (tag_addr,distance))
                    #print("Mobile Tag (0x%04x) Distance: %f mm, Tag Sec dist: %f mm (ALARM ON)" % (tag_addr,distance,tag_sec_distance))
                    return {'ret_type':2}
                else:
                    alarm_flag = 0
                    #print("Mobile Tag (0x%04x) Distance: %f mm, Tag Sec dist: %f mm (ALARM OFF)" % (tag_addr,distance,tag_sec_distance))
                    return {'ret_type':0}
        return {'ret_type':0}
