import serial
import time
import re

pos_devAddr = 0x3FFA

ser = serial.Serial("/dev/ttyS0", 115200, timeout=0.3)

class forklift_position:
    def __init__(self):
        self.x=0
        self.y=0
        self.z=0
        self.qf=0
        self.devAddr=pos_devAddr
        self.tst=round(time.time() * 1000)
        self.init_forklift_position()

    def get_forklift_position(self):
        count = ser.in_waiting
        
        recv_serial = ser.readline()
        
        if(len(recv_serial)>0):

            recv_serial_str = str(recv_serial)
            recv_serial_str.encode('utf-8').strip()

            regex = r"^[-+]?[0-9]+|[-+]?[0-9]+|[-+]?[0-9]+|[-+]?[0-9]+$"

            position_parameters = re.findall(regex, recv_serial_str)
            if (len(position_parameters) == 4): 
                self.x=int(position_parameters[0])
                self.y=int(position_parameters[1])
                self.z=int(position_parameters[2])
                self.qf=int(position_parameters[3])
                self.tst=round(time.time() * 1000)

                #print("time=%d, x=%d, y=%d, z=%d, qf=%d" % (self.tst, self.x,self.y,self.z,self.qf))
        ser.reset_input_buffer()
    def init_forklift_position(self):
        print("connect fork height")
        if ser.is_open == False:
            ser.open()

    def deinit_forklift_position(self):
        print("disconnect fork height")
        if ser != None:
            ser.close()

    if __name__ == '__main__':
        try:
            if ser.is_open == False:
                ser.open()
            getTFminiData()
        except KeyboardInterrupt:   # Ctrl+C
            if ser != None:
                ser.close()