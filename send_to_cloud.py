import paho.mqtt.client as mqtt
import time
import json


class Send_to_cloud_Mqtt:

    def __init__(self):
         self.init_MQTT()

    def init_MQTT(self):
        self.client = mqtt.Client()

        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message

        self.client.username_pw_set(username="tecnologia", password="128Parsecs!")
        self.client.connect("gwqa.revolog.com.br", 1884, 60)
        self.client.loop_start()

    def on_connect(self,client, userdata, flags, rc):
        if rc == 0:
            client.connected_flag = True
            print("Connected to Cloud ")
        else:
            print("Bad connection Returned code=", rc)

  
    def send_message_to_cloud(self,client,position,security_dist,tags_data):
        if(client.connected_flag == True):
            json_data = {}
            json_signals = []
            for row in tags_data:
                temp_signals = {}
                temp_signals["devAddr"] = format(row[0], "04x")
                temp_signals["distance"] = row[1]/1000
                temp_signals["tmst"] = row[2]
                temp_signals["deltaTime"] = row[3]
                json_signals.append(temp_signals)
            json_data["devAddr"] = format(position.devAddr, "04x")
            json_data["secureDistance"] = security_dist/1000
            json_data["tmst"] = position.tst
            json_data["position"] = {"x":position.x/1000,"y":position.y/1000,"z":position.z/1000,"qf":position.qf}
            json_data["signals"] = json_signals
            json_string = json.dumps(json_data)
            print(json_string)
            print("\n")
            self.client.publish("gateway/john_deere/tx", json_string)         

    def on_message(self,client, userdata, message):
        self.process_message(message)
        